# Start a new automator Service project and select "Run Shell Script". Then copy and paste the below into automator. Save it and assign a keyboard shortcut. You'll see why later.

ts=`date +%Y%m%d%H%M%s`
screecapture -i ~/images/$ts-image.png
echo "image::~/images/$ts-image.png[]" | pbcopy